<?php

$calendar_img_url = get_template_directory_uri() . '/assets/calendar-alt-solid.svg';
$linkedin_img_url = get_template_directory_uri() . '/assets/linkedin-in-brands.svg';
$envelope_img_url = get_template_directory_uri() . '/assets/envelope-regular.svg'; 
$calendly_url = get_option('mcs_cl_op');
$linkedin_url = get_option('mcs_li_op');
$emailaddress = get_option('mcs_em_op');
?>

<div id="social">
    <?php if($calendly_url) { ?>
        <a  href="" onclick="Calendly.initPopupWidget({url: '<?php echo $calendly_url; ?>'});return false;">
            <img class="social-icon" src="<?php echo $calendar_img_url; ?>" alt="Maak een afspraak" />
        </a>
    <?php } ?>
    <?php if($linkedin_url) { ?>
    <a href="<?php echo $linkedin_url; ?>">
        <img class="social-icon" src="<?php echo $linkedin_img_url; ?>" alt="LinkedIn" />
    </a>
    <?php } ?>

    <?php if($emailaddress) { ?>
    <a href="mailto:<?php echo $emailaddress; ?>">
        <img id="popup" class="social-icon" src="<?php echo $envelope_img_url; ?>" alt="Contact" />
    </a>
    <?php } ?>


</div>

