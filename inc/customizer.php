<?php
/**
 * Dolphin Theme Theme Customizer
 *
 * @package Dolphin_Theme
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function dolphin_theme_customize_register( $wp_customize ) {


	$wp_customize->add_section('mcs_social_handle', array(
        'title'    => __('Social Network Handles', 'mcs'),
        'description' => 'This shows the top right icons with their link',
        'priority' => 70,
    ));

    //  =============================
    //  = Calendly                  =
    //  =============================
    $wp_customize->add_setting('mcs_cl_op', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('mcs_cl', array(
        'label'      => __('Calendly URL', 'mcs'),
        'section'    => 'mcs_social_handle',
        'settings'   => 'mcs_cl_op',
    ));

	//  =============================
    //  = LinkedIn                  =
    //  =============================
    $wp_customize->add_setting('mcs_li_op', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('mcs_li', array(
        'label'      => __('LinkedIn URL', 'mcs'),
        'section'    => 'mcs_social_handle',
        'settings'   => 'mcs_li_op',
    ));

	//  =============================
    //  = Email                  =
    //  =============================
    $wp_customize->add_setting('mcs_em_op', array(
        'default'        => '',
        'capability'     => 'edit_theme_options',
        'type'           => 'option',

    ));

    $wp_customize->add_control('mcs_em', array(
        'label'      => __('Email address', 'mcs'),
        'section'    => 'mcs_social_handle',
        'settings'   => 'mcs_em_op',
    ));






	
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial(
			'blogname',
			array(
				'selector'        => '.site-title a',
				'render_callback' => 'dolphin_theme_customize_partial_blogname',
			)
		);
		$wp_customize->selective_refresh->add_partial(
			'blogdescription',
			array(
				'selector'        => '.site-description',
				'render_callback' => 'dolphin_theme_customize_partial_blogdescription',
			)
		);
	}

}
add_action( 'customize_register', 'dolphin_theme_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function dolphin_theme_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function dolphin_theme_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function dolphin_theme_customize_preview_js() {
	wp_enqueue_script( 'dolphin-theme-customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), _S_VERSION, true );
}
add_action( 'customize_preview_init', 'dolphin_theme_customize_preview_js' );
