<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Dolphin_Theme
 */

?>

	<footer id="colophon" class="site-footer">
		<div class="site-info">
			 2021 &copy; Geen Poespas <span class="website">| website: <a href="https://zoo.nl">Studio ZOO</a></span>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
	<div id="mobileMenu" class="modal">
	<span class="close">&times;</span>
		<div class="modal-content">
		<a href="/" class="home-mobile">
			<img id="homeicon" class="mobile" src="/wp-content/themes/dolphin-theme/assets/home-solid.svg" style="height:20px;" height="20px" alt="home" />
		</a>
			<?php
			if(is_page(24)) {
				wp_nav_menu(
					array(
						'theme_location' => 'alt_menu',
						'menu_id'        => 'alt_menu',
					)
				);
	
			}
			else {
				wp_nav_menu(
					array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					)
				);
	
			}
				?>
				<?php get_template_part( 'template-parts/social', 'none' ); ?>
		</div>
	</div>
</div><!-- #page -->
<div class="popuptext" style="width:180px;" id="MyPopup">
   
    <span id="contact">Heb je een vraag?</span>
    <span id="close">X</span>
    <span id="contactform" style="display: none;">
        <?php echo do_shortcode('[contact-form-7 id="51" title="Zonder titel"]'); ?>       
    </span>

</div>
<?php wp_footer(); ?>
<link href="https://assets.calendly.com/assets/external/widget.css" rel="stylesheet">
<script src="https://assets.calendly.com/assets/external/widget.js" type="text/javascript" async></script>
</body>
</html>
