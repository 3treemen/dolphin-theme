<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Dolphin_Theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
<style>
body, button, input, select, optgroup, textarea {
	font-family: "Work Sans", sans-serif;
}
</style>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'dolphin-theme' ); ?></a>

	<header id="masthead" class="site-header">
	<?php get_template_part( 'template-parts/social', 'none' ); ?>

		<nav id="site-navigation" class="main-navigation">
		<a class="menu-toggle" id="openMobileMenu">
			<div class="toggle-line"></div>
			<div class="toggle-line"></div>
			<div class="toggle-line"></div>
		</a>
		<a id="logosmall" href="/">
			<img id="homeicon" src="/wp-content/themes/dolphin-theme/assets/home-solid.svg" style="height:20px;" height="20px" alt="home" />
			<img id="imglogosmall" style="display: none;" src="/wp-content/themes/dolphin-theme/assets/logogeenpoespas.png" width="120" height="25" alt="Geen Poespas" />
		</a>
		
			<?php
			if(is_page(24)) {
				wp_nav_menu(
					array(
						'theme_location' => 'alt_menu',
						'menu_id'        => 'alt_menu',
					)
				);
	
			}
			else {
				wp_nav_menu(
					array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					)
				);
	
			}

			
			?>
		</nav><!-- #site-navigation -->
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$dolphin_theme_description = get_bloginfo( 'description', 'display' );
			if ( $dolphin_theme_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $dolphin_theme_description; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->



	</header><!-- #masthead -->
